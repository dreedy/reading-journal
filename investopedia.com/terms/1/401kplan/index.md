---
url: https://www.investopedia.com/terms/1/401kplan.asp
last-read-on: 2020-05-21
---

# [401(k) Plans: The Complete Guide](https://www.investopedia.com/terms/1/401kplan.asp)

## General Notes

- A 401(k) plan is a tax-advantaged (meaning there are tax benefits), defined contribution retirement account
- It’s named after a section of the U.S. Internal Revenue Code
- Traditional 401(k), contributions are pre-tax so you are taxed when you withdraw funds
- Roth 401(k), contributions are after-tax so withdrawals “can be” tax-free
  - Sometimes referred to as a “designated Roth account”
- Contribution limits:
  - Adjusted periodically to account for inflation
  - As of 2020 $19,500/year (under age 50)
  - Even if you have both, you cannot exceed the limit ($19,500 total) for a given year
- It’s important to note that employer contributions can only go into a Traditional 401(k) account!
- Don’t put all your eggs in this one basket, it can be difficult to withdraw early or extra
- Must be at least age 59 ½ to start making withdrawals
- After age 72, you _must_ withdraw at least a specific percentage of account (unless it is still housed under your current employer)
- Roth 401(k)s became a thing in 2006!
- When you leave a company where you have a 401(k) plan, you generally have four options:
  1. Withdraw the money (taxable & could incur 10% early distribution fee)
  2. Roll it over into an IRA – government has strict rules; must be done within 60 days
  3. Leave it with the previous employer – can’t make any further contributions to it
  4. Move it to a new employer

## Questions

- [x] Why the need for the “can be tax-free” language in the case of Roth 401(k) withdrawals?
  - **A:** See article on [Qualified Distribution](https://www.investopedia.com/terms/q/qualifieddistribution.asp)

## To-dos

- [ ] …

## Comments

- …

## Miscellaneous

- …
