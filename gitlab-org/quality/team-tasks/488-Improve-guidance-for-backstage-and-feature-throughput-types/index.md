---
url: https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/488
last-read-on: 2020-05-20
---

# [Improve guidance for backstage and feature throughput types](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/488#note_332937152)

## Questions

- [ ] Should we consider adding an ~"experiment" type label in place of or in addition to the ~"feature flag" one?
  - Seems like nearly everything should be behind a feature flag, at first, at least. So maybe getting rid of that one in favor of ~"feature" + ~"experiment".
  - **A:** …

## To-dos

- [ ] …

## Comments

- I like the idea of ~"feature::groundwork", ~"feature::backend", & ~"feature::frontend" (add both of these last two if it is fullstack?).
- Some terminology that might be useful to adopt: “Structure” & “Behavior.” “Structure” for all of the things that are not seen/experienced by users & “Behavior” for all of the things that are.
  - I recently watched [Kent Beck’s presentation from RailsConf 2020](https://railsconf.com/2020/video/kent-beck-tidy-first) where he talks about breaking things down into super simple, super small pieces based on whether they are addressing “structure” or “behavior.”

## Miscellaneous

- …
